package com.innominds.Unitassignment;

public class SameDirectionException extends RuntimeException { 
	public SameDirectionException(String msg) {
		super(msg);
	}
}
