package com.innominds.Unitassignment;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.DeflaterOutputStream;
import java.io.IOException;

public class Question26Compress {
	public static void main(String args[]) throws IOException {
	      //Instantiating the FileInputStream
	      String inputPath = "C:\\Users\\ssaideo\\Pictures\\ssd.txt";
	      FileInputStream inputStream = new FileInputStream(inputPath);
	      //Instantiating the FileOutputStream
	      String outputPath = "C:\\Users\\ssaideo\\Pictures\\ssd.txt";
	      FileOutputStream outputStream = new FileOutputStream(outputPath);
	      //Instantiating the DeflaterOutputStream
	      DeflaterOutputStream compresser = new DeflaterOutputStream(outputStream);
	      int contents;
	      while ((contents=inputStream.read())!=-1){
	         compresser.write(contents);
	      }
	      compresser.close();
	      System.out.println("File compressed.......");
	   }
	}

