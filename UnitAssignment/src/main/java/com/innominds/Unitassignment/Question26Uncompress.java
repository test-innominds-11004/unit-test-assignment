package com.innominds.Unitassignment;


	import java.io.File;
	import java.io.FileInputStream;
	import java.io.FileOutputStream;
	import java.io.IOException;
	import java.util.zip.InflaterInputStream;
	public class Question26Uncompress {

	   public static void main(String args[]) throws IOException {
	      StringinputPath ="C:\\Users\\ssaideo\\Pictures\\ssd.txt";
	      //Instantiating the FileInputStream
	      FileInputStream inputStream = new FileInputStream(inputPath);
	      String outputpath = "C:\\Users\\ssaideo\\Pictures\\ssd.txt";
	      FileOutputStream outputStream = new FileOutputStream(outputpath);
	      InflaterInputStream decompresser = new InflaterInputStream(inputStream);
	      int contents;
	      while ((contents=decompresser.read())!=-1){
	         outputStream.write(contents);
	      }
	      //close the file
	      outputStream.close();
	      decompresser.close();
	      System.out.println("File un-compressed.......");
	   }
	}

