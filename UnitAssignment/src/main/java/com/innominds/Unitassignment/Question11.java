package com.innominds.Unitassignment;

public class Question11{
	private double distance;
	private int time;
	private double speed;
	private String direction;
	private int route;
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public int getRoute() {
		return route;
	}
	public void setRoute(int route) {
		this.route = route;
	}
	public Question11(double distance, int time, double speed, String direction, int route) {
		super();
		this.distance = distance;
		this.time = time;
		this.speed = speed;
		this.direction = direction;
		this.route = route;
	}
	public static boolean  avoidcollision(Question11 v1,Question11 v2) {
		boolean flag;
		try {
			if(v1.getDirection().equals(v2.getDirection())) {
				System.out.println("Vehicles are going in same direction therefore no collision");
				flag=true;
			}
			else {
				flag=false;
				throw new SameDirectionException("Vehicles are running in opposite direction there is collision");
			}
		}	catch (SameDirectionException e) {
				System.out.println(e.getMessage());
				v1.setRoute(2);
				flag=true;
				if (v1.getRoute()==v1.getRoute()) {
					System.out.println("Vehicles are running in opposite direction and route has been done therefore there will be no collision");
					
				}}
				catch (Exception e) {
					System.out.println(e);
					flag= false;
				}
			
				return flag;
			}
			public static void main(String args[]) {
				Question11 v1=new  Question11(2,2,50,"Right",2);
				Question11 v2=new  Question11(2,2,50,"Left",2);
				avoidcollision(v1,v2);
			}
		}
	
