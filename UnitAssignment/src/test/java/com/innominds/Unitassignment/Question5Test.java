package com.innominds.Unitassignment;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
	public class Question5Test
	{
		MyThread m1=new MyThread("MyThread -1", 2);
		MyThread m2=new MyThread("MyThread -2", 3);
		MyThread m3=new MyThread("MyThread -3", 4);
		MyThread m4=new MyThread("MyThread -4", 8);
		MyThread m5=new MyThread("MyThread -5", 9);
		
		@Test
		public void multithread() 
		{
			m1.setPriority(2);
			m2.setPriority(3);
			m3.setPriority(4);
			m4.setPriority(8);
			m5.setPriority(9);
			m1.setName("1st Thread");
			m2.setName("2nd Thread");
			m3.setName("3rd Thread");
			m4.setName("4th Thread");
			m5.setName("5th Thread");
			m1.start();
			assertTrue(m1.isAlive());
			m2.start();
			System.out.println(Thread.currentThread());
			assertTrue(m2.isAlive());
			m3.start();
			assertTrue(m3.isAlive());
			m4.start();
			assertTrue(m4.isAlive());
			m5.start();
			assertTrue(m5.isAlive());
		}
	}

