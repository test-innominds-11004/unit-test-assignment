package com.innominds.Unitassignment;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class Question23EmployeeTest {
	@Test
	void testEmployeeList() {

		ArrayList< Question23Employee> empList1 = new ArrayList<>();
		ArrayList< Question23Employee> empList2 = new ArrayList<>();

		empList1.add(new  Question23Employee(1, "Siddhant", "Java")); 
		empList1.add(new  Question23Employee(2, "Snehith", "Java"));
		empList1.add(new  Question23Employee(3, "Aameer", "Java"));
		
		empList2.add(new  Question23Employee(3, "Aameer", "Java")); 
		empList2.add(new  Question23Employee(1, "Siddhant", "Java"));
		empList2.add(new  Question23Employee(2, "Snehith", "Java"));

		assertEquals(empList2,  Question23Employee.sortEmployee(empList1));
	}
	}
