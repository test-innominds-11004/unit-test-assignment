package com.innominds.Unitassignment;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question11Test {

	@Test
	void testSameDirection() {
		Question11 v1=new  Question11(2,2,50,"Right",2);
		Question11 v2=new  Question11(2,2,50,"Right",2);
	assertTrue(Question11.avoidcollision(v1, v2));

	}

@Test
void testOppositeDirection() {
	Question11 v1=new  Question11(2,2,50,"Right",2);
	Question11 v2=new  Question11(2,2,50,"Left",2);
assertTrue(Question11.avoidcollision(v1, v2));
}
}
