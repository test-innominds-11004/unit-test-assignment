package com.innominds.Unitassignment;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Question4Test {
	@Test
	public void daemonThread()
	{
	Question4 t1=new Question4();
	t1.setDaemon(true);
	t1.start();
	assertEquals(true, t1.isDaemon());
	}

	@Test
	public void notDaemonThread()
	{
		Question4 t1=new Question4();
	t1.start();
	assertNotEquals(true, t1.isDaemon());
	}

	@Test
	public void DaemonThread()
	{
		Question4 t1=new Question4();
		Question4 t2=new Question4();
	t1.start();
	t2.setDaemon(true);
	t2.start();
	assertEquals(true, t2.isDaemon());
	assertEquals(false, t1.isDaemon());
	}

	

		
		
		
		  
		  
		  

		
	}


