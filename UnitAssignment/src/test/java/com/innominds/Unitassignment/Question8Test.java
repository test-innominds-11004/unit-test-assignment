package com.innominds.Unitassignment;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.innominds.Unitassignment.Question8;

class Question8Test {

	@Test
	void testevenNumbersList() {
		List<Integer> evenNumbersList= new ArrayList<Integer>();
		evenNumbersList.add(2);
		evenNumbersList.add(4);
		evenNumbersList.add(6);
		evenNumbersList.add(8);
		evenNumbersList.add(10);
		Question8 question8=new Question8();
		assertEquals(evenNumbersList,question8.getEvenNumbers()) ;
	}
		

}
